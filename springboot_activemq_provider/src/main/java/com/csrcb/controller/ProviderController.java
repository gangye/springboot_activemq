package com.csrcb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 * @Classname ProviderController
 * @Date 2020/6/9 10:33
 * @Created by gangye
 */
@RestController
@RequestMapping(value = "/sendMessage")
public class ProviderController {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Queue queue;

    @Autowired
    private Topic topic;

    //发送消息，destination是发送到的队列，message是待发送的消息
    private void sendMessage (Destination destination,final String message){
        jmsMessagingTemplate.convertAndSend(destination,message);
    }

    @PostMapping(value = "/queue/send")
    public String sendQueue(@RequestBody String text){
        //queue时候的持久化
//        jmsTemplate.setDeliveryMode(2);
//        jmsTemplate.setExplicitQosEnabled(true);
//        jmsTemplate.setDeliveryPersistent(true);
//        jmsTemplate.convertAndSend(this.queue,text);
        sendMessage(this.queue,text);
        return "success";
    }

    @PostMapping(value = "/topic/send")
    public String sendTopic(@RequestBody String text){
        sendMessage(this.topic,text);
        return "success";
    }
}
