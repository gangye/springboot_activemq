package com.csrcb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

/**
 * @Classname ActiveMqProviderApplication
 * @Description 消息队列生产者启动类
 * @Date 2020/6/8 17:21
 * @Created by gangye
 */
@SpringBootApplication
//启动消息队列
@EnableJms
public class ActiveMqProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ActiveMqProviderApplication.class,args);
    }
}
