# springboot_activemq

一.ActiveMQ的安装启动
http://archive.apache.org/dist/activemq
下载文件后解压在bin目录下使用cmd指令执行activemq.bat start启动，在浏览器输入http://localhost:8161/admin（用户名和密码默认为admin）


tips：8161是后台管理系统，61616是给java用的tcp端口。

解压缩后在文件的bin目录下执行cmd指令：
activemq.bat start

二.springboot整合ActiveMq
1.建立springboot项目，引入pom依赖
 
2.在生产者和消费者定义配置文件

```
#生产者的服务的端口号
server.port=7001

spring.activemq.broker-url=tcp://localhost:61616
spring.activemq.user=admin
spring.activemq.password=admin
#在考虑结束之前等待的时间
spring.activemq.close-timeout=15s
# 默认代理URL是否应该在内存中。如果指定了显式代理，则忽略此值。
spring.activemq.in-memory=true
# 是否在回滚回滚消息之前停止消息传递。这意味着当启用此命令时，消息顺序不会被保留。
spring.activemq.non-blocking-redelivery=false
# 等待消息发送响应的时间。设置为0等待永远。
spring.activemq.send-timeout: 0
spring.activemq.queue-name: active.queue
spring.activemq.topic-name: active.topic.name.model

spring.activemq.pool.enabled=true
#连接池最大连接数
spring.activemq.pool.max-connections=10
#空闲的连接过期时间，默认为10秒
spring.activemq.pool.idle-timeout=30s
#消息模式 true:广播(Topic)，false:队列(Queue),默认时false,若要使用topic模式需要配置下面配置,可以再代码中自定义配置
#spring.jms.pub-sub-domain=true
# 是否信任所有包
#spring.activemq.packages.trust-all=
# 要信任的特定包的逗号分隔列表（当不信任所有包时）
#spring.activemq.packages.trusted=
# 当连接请求和池满时是否阻塞。设置false会抛“JMSException异常”。
#spring.activemq.pool.block-if-full=true
# 如果池仍然满，则在抛出异常前阻塞时间。
#spring.activemq.pool.block-if-full-timeout=-1ms
# 是否在启动时创建连接。可以在启动时用于加热池。
#spring.activemq.pool.create-connection-on-startup=true
# 是否用Pooledconnectionfactory代替普通的ConnectionFactory。
#spring.activemq.pool.enabled=false
# 连接过期超时。
#spring.activemq.pool.expiry-timeout=0ms
# 连接空闲超时
#spring.activemq.pool.idle-timeout=30s
# 连接池最大连接数
#spring.activemq.pool.max-connections=1
# 每个连接的有效会话的最大数目。
#spring.activemq.pool.maximum-active-session-per-connection=500
# 当有"JMSException"时尝试重新连接
#spring.activemq.pool.reconnect-on-exception=true
# 在空闲连接清除线程之间运行的时间。当为负数时，没有空闲连接驱逐线程运行。
#spring.activemq.pool.time-between-expiration-check=-1ms
# 是否只使用一个MessageProducer
#spring.activemq.pool.use-anonymous-producers=trues
```


3.生产者启动类

```
/**
 * @Classname ActiveMqProviderApplication
 * @Description 消息队列生产者启动类
 * @Date 2020/6/8 17:21
 * @Created by gangye
 */
@SpringBootApplication
//启动消息队列
@EnableJms
public class ActiveMqProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ActiveMqProviderApplication.class,args);
    }
}
```

 
4.在生产者项目和消费者项目分别编写BeanConfig.java自定义配置类

```
/**
 * @Classname BeanConfig
 * @Description 初始化和配置 ActiveMQ 的连接
 * @Date 2020/6/8 17:29
 * @Created by gangye
 */
@Configuration
public class BeanConfig {
    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Value("${spring.activemq.user}")
    private String username;

    @Value("${spring.activemq.password}")
    private String password;

    @Value("${spring.activemq.queue-name}")
    private String queueName;

    @Value("${spring.activemq.topic-name}")
    private String topicName;

    @Bean(name = "queue")
    public Queue queue(){
        return new ActiveMQQueue(queueName);
    }

    @Bean(name = "topic")
    public Topic topic(){
        return new ActiveMQTopic(topicName);
    }

    @Bean
    public ConnectionFactory connectionFactory(){
        return new ActiveMQConnectionFactory(username,password,brokerUrl);
    }

    @Bean
    public JmsMessagingTemplate jmsMessagingTemplate(){
        return new JmsMessagingTemplate(connectionFactory());
    }

    //在Queue模式中，对消息的监听需要对containerFactory进行配置
    @Bean("queueListener")
    public JmsListenerContainerFactory<?> queueJmsListenerContainerFactory(ConnectionFactory connectionFactory){
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setPubSubDomain(false);
        return factory;
    }

    //在Topic模式中，对消息的监听需要对containerFactory进行配置
    @Bean("topicListener")
    public JmsListenerContainerFactory<?> topicJmsListenerContainerFactory(ConnectionFactory connectionFactory){
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setPubSubDomain(true);
        return factory;
    }
}
```


5.编写生产者的路由

```
@RestController
@RequestMapping(value = "/sendMessage")
public class ProviderController {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Queue queue;

    @Autowired
    private Topic topic;

    //发送消息，destination是发送到的队列，message是待发送的消息
    private void sendMessage (Destination destination,final String message){
        jmsMessagingTemplate.convertAndSend(destination,message);
    }

    @PostMapping(value = "/queue/send")
    public String sendQueue(@RequestBody String text){
        sendMessage(this.queue,text);
        return "success";
    }

    @PostMapping(value = "/topic/send")
    public String sendTopic(@RequestBody String text){
        sendMessage(this.topic,text);
        return "success";
    }
}
```


6.在消费者中分别编写queue模式以及topic模式的消费者

```
@Slf4j
@Component
public class QueueConsumerListener {
    //queue模式的消费者
    @JmsListener(destination = "${spring.activemq.queue-name}",containerFactory = "queueListener")
    public void readActiveQueue(String message){
        log.info("queue接受到消息内容:{}",message);
        System.out.println("queue接受的消息内容："+message);
    }
}
```




```
@Slf4j
@Component
public class TopicConsumerListener {
    //topic模式的消费者
    @JmsListener(destination = "${spring.activemq.topic-name}",containerFactory = "topicListener")
    public void readActiveTopic(String message){
        log.info("topic接受到消息内容:{}",message);
        System.out.println("topic接受的消息内容："+message);
    }
}
```


7.启动生产者和消费者项目
使用psotman生产一条数据(queue)
 
查看消费者的日志：
 
同时在activemq的管理端可以看到数据：
 

再使用生产者生产一条topic的消息
 
消费者的日志及控制台：
 
activemq的浏览器管理端：
 

三.持久化queue以及topic的操作
1.queue模式下开启持久化
在生产者项目中添加这三行代码

```
jmsTemplate.setDeliveryMode(2);
jmsTemplate.setExplicitQosEnabled(true);
jmsTemplate.setDeliveryPersistent(true);
```

 

2. 开启主题持久化,配置类添加如下配置

```
//topic持久化使用的配置
@Bean(name = "topicListenerFactory")
public JmsListenerContainerFactory<DefaultMessageListenerContainer> topicListenerFactory(ConnectionFactory connectionFactory){
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    factory.setSubscriptionDurable(true);// Set this to "true" to register a durable subscription,
    factory.setClientId("B");
    factory.setConnectionFactory(connectionFactory);
    return factory;
}
```


在消费者服务中：

```
//topic模式持久化
//消费者消费  destination队列或者主题的名字
@JmsListener(destination = "${spring.activemq.topic-name}",containerFactory = "topicListenerFactory")
public void getMessage(TextMessage message, Session session) throws JMSException {
    log.info("消费者获取到消息："+message.getText());
}
```


 **这里需要注意,主题的数据不会被消费,会被一直记录下来,只能手动清除** 

