package com.csrcb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

/**
 * @Classname ActiveMqConsumerApplication
 * @Description 消费者启动类
 * @Date 2020/6/9 11:08
 * @Created by gangye
 */
@SpringBootApplication
@EnableJms
public class ActiveMqConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ActiveMqConsumerApplication.class,args);
    }
}
