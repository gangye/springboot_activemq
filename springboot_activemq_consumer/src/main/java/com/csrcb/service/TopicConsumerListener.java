package com.csrcb.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * @Classname TopicConsumerListener
 * @Date 2020/6/9 14:07
 * @Created by gangye
 */
@Slf4j
@Component
public class TopicConsumerListener {
    //topic模式的消费者
    @JmsListener(destination = "${spring.activemq.topic-name}",containerFactory = "topicListener")
    public void readActiveTopic(String message){
        log.info("topic接受到消息内容:{}",message);
        System.out.println("topic接受的消息内容："+message);
    }

//    //topic模式持久化
//    //消费者消费  destination队列或者主题的名字
//    @JmsListener(destination = "${spring.activemq.topic-name}",containerFactory = "topicListenerFactory")
//    public void getMessage(TextMessage message, Session session) throws JMSException {
//        log.info("消费者获取到消息："+message.getText());
//    }
}
