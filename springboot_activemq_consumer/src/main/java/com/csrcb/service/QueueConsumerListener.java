package com.csrcb.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @Classname QueueConsumerListener
 * @Date 2020/6/9 14:03
 * @Created by gangye
 */
@Slf4j
@Component
public class QueueConsumerListener {
    //queue模式的消费者
    @JmsListener(destination = "${spring.activemq.queue-name}",containerFactory = "queueListener")
    public void readActiveQueue(String message){
        log.info("queue接受到消息内容:{}",message);
        System.out.println("queue接受的消息内容："+message);
    }
}
